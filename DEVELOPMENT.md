# Development

## Dev Requirements
- `poetry`
- `python 3.9` (recommended to use `pyenv` for using multiple python executables)

## Install requirements
```bash
$ poetry install
```

## Run

To generate booklets run these commands

```bash
$ poetry shell  # activate python environment
$ python main.py
```

It will generate booklets in `dist/` folder.

You can tweak `src/config.py` for your needs.
