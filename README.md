# **Monster Hunter: World** booklet by **Hulvdan**

Printable booklet that shows weaknesses/attack types of bosses.

Based on **Tickthokk's** PDF version [[Reddit Post](https://www.reddit.com/r/MonsterHunterWorld/comments/7wfvw3/pdf_wyverns_you_your_guide_to_monster_weaknesses/)]

I created it and updated after 2 years to look at my Python
programming progress after 14 months of commercial experience.
Also, I wanted to reinforce my CI skill.

If you have any recommendations *be sure to open an issue* at
[Gitlab](https://gitlab.com/Hulvdan/mhw-booklet/-/issues).

# Links
- [Releases](https://gitlab.com/Hulvdan/mhw-booklet/-/releases). `Archive of booklets` in `other` section
- GitLab repository. [[GitLab](https://gitlab.com/Hulvdan/mhw-booklet)]
- Current Reddit Post. [[Reddit](https://www.reddit.com/r/MonsterHunterWorld/comments/njj57i/mhw_printable_monsters_weaknesses_guide_updated/)]
- Old reddit post. [[Reddit](https://www.reddit.com/r/MonsterHunterWorld/comments/98avyb/mhw_printable_monsters_weaknesses_guide/)]
- Old repository. Outdated! [[GitHub](https://github.com/Hulvdan/mhw_booklet)]

# Development

Look at `DEVELOPMENT.md`
