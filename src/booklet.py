import os
from enum import Enum, unique
from itertools import count
from math import ceil
from pathlib import Path
from typing import ClassVar, Generator, List

from PIL import Image
from progress.bar import IncrementalBar

from . import colors
from .config import (
    BAR_MESSAGE_LENGTH, CARD_SIZE, CARDS_HORIZONTAL_PADDING,
    CARDS_VERTICAL_PADDING, DIST_FOLDER, logger)
from .helper import alpha_paster
from .images import Images
from .library import Library
from .monster_card import MonsterCard
from .types import Color


@unique
class FillingMode(Enum):
    left_to_right = 0
    top_to_bottom = 1


class Booklet:
    """Image filled with monsters."""

    filling_color: ClassVar[Color] = colors.WHITE

    def __init__(self,
                 columns: int,
                 rows: int,
                 filling_mode: FillingMode):
        """Initialization.

        Args:
            columns (int): Number of columns on the sheet.
            rows (int): Number of rows on the sheet.
            filling_mode (FillingMode): [description]
        """
        self._columns = columns
        self._rows = rows

        self._filling_mode = filling_mode

        logger.info('Creating monster cards...')

        monsters_library = Library.get_instance()

        self._cards: List[MonsterCard] = []
        for monster_data in monsters_library:
            card = MonsterCard(monster_data, CARD_SIZE[0], CARD_SIZE[1])
            self._cards.append(card)

    def export_as_png(self, export_filename_wo_ext: str) -> None:
        """Export booklet to png file.

        Args:
            export_filename_wo_ext: Export filename without extension.
        """
        logger.info('Exporting booklet as png...')

        # Caching images...
        Images.get_instance()

        sheet_width = (CARD_SIZE[0] * self._columns +
                       CARDS_HORIZONTAL_PADDING * (self._columns - 1))
        sheet_height = (CARD_SIZE[1] * self._rows +
                        CARDS_VERTICAL_PADDING * (self._rows - 1))
        sheet_size = (sheet_width, sheet_height)

        # Progress bar with len(monsters) + sheets_count steps
        progress_bar_message = 'Loading "{}"'
        progress_bar = IncrementalBar(
            progress_bar_message.format(self._cards[0].name).ljust(
                BAR_MESSAGE_LENGTH, ' '),
            max=len(self._cards) + self._sheets_count,
            suffix='%(index)d/%(max)d, elapsed: %(elapsed)d sec.')

        export_filename_generator = self._gen_export_path(
            export_filename_wo_ext)
        for sheet_index in range(self._sheets_count):
            sheet = Image.new('RGBA', sheet_size, self.filling_color)

            stop_iterating = False
            for row_index in range(self._rows):
                for col_index in range(self._columns):
                    card_index = self._card_index(
                        col_index, row_index, sheet_index)
                    if card_index >= len(self._cards):
                        stop_iterating = True
                        break

                    card_x = col_index * (CARD_SIZE[0] +
                                          CARDS_HORIZONTAL_PADDING)
                    card_y = row_index * (CARD_SIZE[1] +
                                          CARDS_VERTICAL_PADDING)
                    card_position = (card_x, card_y)

                    current_card = self._cards[card_index]
                    progress_bar.message = progress_bar_message.format(
                        current_card.name).ljust(BAR_MESSAGE_LENGTH, ' ')
                    card_image = current_card.get_card_image()
                    alpha_paster(sheet, card_image, card_position)
                    progress_bar.next(1)  # noqa: B305

                if stop_iterating:
                    break

            if not os.path.exists(DIST_FOLDER):
                os.mkdir(DIST_FOLDER)

            export_file_path = next(export_filename_generator)
            progress_bar.message = 'Exporting booklet to "{}"'.format(
                export_file_path)
            progress_bar.next(1)  # noqa: B305
            sheet.save(export_file_path)

        progress_bar.finish()
        logger.info('Booklet exported!')

    def _gen_export_path(
            self,
            export_filename_wo_ext: str) -> Generator[Path, None, None]:
        """Generate export file's path.

        Args:
            export_filename_wo_ext: Export filename without extension.

        Yields:
            Path to file.
        """
        if self._sheets_count == 1:
            yield DIST_FOLDER / f'{export_filename_wo_ext}.png'

        yield from (DIST_FOLDER / f'{export_filename_wo_ext}-{sheet_number}.png'
                    for sheet_number in count(1))

    @property
    def _count_monsters_in_sheet(self) -> int:
        return self._rows * self._columns

    @property
    def _sheets_count(self) -> int:
        return ceil(len(self._cards) / self._count_monsters_in_sheet)

    def _card_index(self,
                    col_index: int,
                    row_index: int,
                    sheet_index: int) -> int:
        if self._filling_mode == FillingMode.left_to_right:
            return (row_index * self._columns +
                    col_index +
                    self._count_monsters_in_sheet * sheet_index)
        elif self._filling_mode == FillingMode.top_to_bottom:
            return (col_index * self._rows +
                    row_index +
                    self._count_monsters_in_sheet * sheet_index)
        raise ValueError(f'Wrong filling mode "{self._filling_mode}"!')
