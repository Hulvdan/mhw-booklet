TRANSPARENT = (0, 0, 0, 0)
BLACK = '#000000'
WHITE = (255, 255, 255, 255)
FONT_POISON = '#81007F'
FONT_SLEEP = '#05006C'
FONT_PARALYSIS = '#666500'
FONT_BLAST = '#431900'
FONT_STUN = '#666500'
BRACES_COLOR = (100, 100, 100, 255)
BRACES_COLOR_FADED = (200, 200, 200, 255)
