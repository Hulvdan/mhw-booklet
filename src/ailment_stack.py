from enum import Enum, unique

from PIL import Image, ImageDraw, ImageFont  # noqa: WPS102

from . import colors
from .config import (
    AILMENT_FONT_SIZE, BLANK_SPACE_IF_AILMENT_IS_ZERO, FONT_FOLDER,
    WEAKNESS_COLUMN_WIDTH, WEAKNESS_COLUMN_WIDTH_SMALL, WEAKNESS_ROW_PADDING,
    WEAKNESS_ROW_PADDING_SMALL, logger)
from .helper import alpha_paster
from .images import Images
from .types import Ailments


@unique
class AilmentType(Enum):
    poison = 0
    sleep = 1
    paralysis = 2
    blast = 3
    stun = 4


class AilmentStack:
    def __init__(self, powers: Ailments):
        self._powers = powers

        self._max_power = max(powers)

        self._poison_ail = powers[0]
        self._sleep_ail = powers[1]
        self._paralysis_ail = powers[2]
        self._blast_ail = powers[3]
        self._stun_ail = powers[4]

    def get_image(self) -> Image.Image:
        """Image of ailments with their powers stacked in column.

        Returns:
            Image of ailments.
        """
        images_instance = Images.get_instance()
        size = (self.width, self.height)
        small_size = (WEAKNESS_COLUMN_WIDTH_SMALL, WEAKNESS_COLUMN_WIDTH_SMALL)
        placeholder_image = Image.new('RGBA', size, colors.TRANSPARENT)

        images_by_powers = (images_instance.ail_poison,
                            images_instance.ail_sleep,
                            images_instance.ail_paralysis,
                            images_instance.ail_blast,
                            images_instance.ail_stun)
        colors_by_powers = (colors.FONT_POISON,
                            colors.FONT_SLEEP,
                            colors.FONT_PARALYSIS,
                            colors.FONT_BLAST,
                            colors.FONT_STUN)

        # Don't draw anything if monster is immune to ailments
        monster_is_immune_to_ailments = all(
            power == 0 for power in self._powers)
        if monster_is_immune_to_ailments:
            logger.debug('Created Ailment Stack image')
            return placeholder_image

        # Draw 'Ailment' icon
        alpha_paster(
            placeholder_image,
            images_instance.ail_ailment.resize((self.width, self.width)))
        last_y = WEAKNESS_ROW_PADDING + WEAKNESS_COLUMN_WIDTH

        # Draw icons
        for i in range(5):
            # Leave blank space if BLANK_SPACE_IF_AILMENT_IS_ZERO=True
            if self._powers[i] == 0 and (not BLANK_SPACE_IF_AILMENT_IS_ZERO):
                continue

            if self._powers[i] > 0:
                alpha_paster(placeholder_image,
                             images_by_powers[i].resize(small_size),
                             (0, last_y))
            last_y += WEAKNESS_COLUMN_WIDTH_SMALL + WEAKNESS_ROW_PADDING_SMALL

        # Draw numbers
        drawer = ImageDraw.Draw(placeholder_image)
        font = ImageFont.truetype(
            str(FONT_FOLDER / '11988.otf'), AILMENT_FONT_SIZE)
        last_y = WEAKNESS_ROW_PADDING + WEAKNESS_COLUMN_WIDTH
        for i in range(5):
            if not BLANK_SPACE_IF_AILMENT_IS_ZERO:
                continue
            if self._powers[i] > 0:
                # Drawing power of ailment.
                number_pos = [small_size[0], last_y - 24]
                if self._powers[i] == 1:
                    # Spacing "1" by 3 pixels to the right for perfectionism.
                    number_pos[0] += 3

                drawer.text(number_pos,
                            str(self._powers[i]),
                            font=font,
                            fill=colors_by_powers[i])
            last_y += (WEAKNESS_COLUMN_WIDTH_SMALL +
                       WEAKNESS_ROW_PADDING_SMALL)

        logger.debug('Created Ailment Stack image')
        return placeholder_image

    @property
    def width(self) -> int:
        return WEAKNESS_COLUMN_WIDTH

    @property
    def height(self) -> int:
        icons_sum = WEAKNESS_COLUMN_WIDTH * 4
        paddings_sum = WEAKNESS_ROW_PADDING * 3
        return icons_sum + paddings_sum
