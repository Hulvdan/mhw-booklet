import re
from functools import lru_cache
from typing import Any, List

import commentjson
from schema import And, Schema

from .config import LIBRARY_DATA_PATH, logger
from .types import MonsterData

LARGE_MONSTERS_COUNT = 71


def _validate_color(color: str):
    if color == '#':
        logger.warning('Found undefined name color!')
        return True
    match = re.match('#[a-fA-F0-9]{6}', color)
    if match is None:
        return False
    return match.group(0) == color


class Library:
    _weakness_type = [int, [int, int]]
    _attack_type = [And(str, len)]
    _ailments_type = [int]

    library_schema = Schema(And([
        {
            'name': And(str, len),
            'attack': And(_attack_type,
                          lambda attack: len(attack) <= 6),
            'weakness': And(_weakness_type,
                            lambda weakness: len(weakness) == 5),
            'ailments': And(_ailments_type,
                            lambda value: len(value) == 5),
            'color': And(str, _validate_color)
        }, lambda monsters: len(monsters) == LARGE_MONSTERS_COUNT
    ]))

    @classmethod
    @lru_cache(1)
    def get_instance(cls) -> List[MonsterData]:
        """Get Data related to monsters for booklet. Using singleton pattern."""
        logger.info('Loading library "%s"...' % LIBRARY_DATA_PATH)
        with open(str(LIBRARY_DATA_PATH)) as data_file:
            return cls._validate_data(commentjson.load(data_file))

    @classmethod
    def _validate_data(cls, monsters_data: Any) -> List[MonsterData]:
        logger.info('Validating library...')
        if not cls.library_schema.validate(monsters_data):
            logger.error('Incorrect library!')
            raise ValueError('Incorrect library!')
        logger.info('Library is valid.')
        return monsters_data
