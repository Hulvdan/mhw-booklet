from pathlib import Path
from typing import Optional, Tuple

from PIL import Image, ImageEnhance

from .config import (
    ELEMENTS_FOLDER, FADED_IMAGE_OPACITY, WEAKNESS_COLUMN_WIDTH,
    WEAKNESS_COLUMN_WIDTH_SMALL, logger)

_default_img_size = (WEAKNESS_COLUMN_WIDTH, WEAKNESS_COLUMN_WIDTH)


def small_image_of_element_weakness(image: Image.Image):
    """Get small version of an image."""
    width = WEAKNESS_COLUMN_WIDTH_SMALL
    return image.resize((width, width))


def fade_image(image: Image.Image, fade=True):
    """Get faded version of an image."""
    if not fade:
        return image
    background = Image.new('RGB', image.size, (255, 255, 255))
    background.paste(image, mask=image.split()[3])
    background.putalpha(FADED_IMAGE_OPACITY)
    return background


def load_image(
        image_path: Path,
        image_size: Tuple[int, int] = _default_img_size) -> Image.Image:
    """Getting thumbnailed antialiased image with configured size."""
    logger.debug('Loading image "%s" and resizing it to (%d, %d)' %
                 (image_path, image_size[0], image_size[1]))
    img: Image.Image = Image.open(image_path)
    if img.mode != 'RGBA':
        img = img.convert('RGBA')
    img.thumbnail(image_size, Image.ANTIALIAS)
    return img


class Images:  # noqa: WPS230
    """Loading all images except monsters."""

    __instance__: Optional['Images'] = None

    def __init__(self):
        """Loading images from filesystem."""
        # Image scaling needed to put 5 little stars into height of 3 normal
        # stars. Spacing calculated too
        self.elem_fire = load_image(ELEMENTS_FOLDER / 'fire.png')
        self.elem_water = load_image(ELEMENTS_FOLDER / 'water.png')
        self.elem_thunder = load_image(ELEMENTS_FOLDER / 'thunder.png')
        self.elem_ice = load_image(ELEMENTS_FOLDER / 'ice.png')
        self.elem_dragon = load_image(ELEMENTS_FOLDER / 'dragon.png')

        self.fireblight = load_image(ELEMENTS_FOLDER / 'fireblight.png')
        self.waterblight = load_image(ELEMENTS_FOLDER / 'waterblight.png')
        self.iceblight = load_image(ELEMENTS_FOLDER / 'iceblight.png')
        self.thunderblight = load_image(ELEMENTS_FOLDER / 'thunderblight.png')
        self.dragonblight = load_image(ELEMENTS_FOLDER / 'dragonblight.webp')
        self.diamond = load_image(ELEMENTS_FOLDER / 'diamond.png')
        self.mud = load_image(ELEMENTS_FOLDER / 'mud.png')
        self.noxious_poison = load_image(ELEMENTS_FOLDER / 'noxious_poison.png')
        self.blastscourge = load_image(ELEMENTS_FOLDER / 'blastscourge.png')
        self.defense_down = load_image(ELEMENTS_FOLDER / 'defense_down.png')
        self.blastblight = load_image(ELEMENTS_FOLDER / 'blastblight.webp')

        self.ail_poison = load_image(ELEMENTS_FOLDER / 'poison.png')
        self.ail_sleep = load_image(ELEMENTS_FOLDER / 'sleep.png')
        self.ail_paralysis = load_image(ELEMENTS_FOLDER / 'paralysis.png')
        self.ail_blast = load_image(ELEMENTS_FOLDER / 'blast.png')
        self.ail_stun = load_image(ELEMENTS_FOLDER / 'stun.png')

        self.ail_bleeding = load_image(ELEMENTS_FOLDER / 'bleeding.png')
        self.ail_effluvial = load_image(ELEMENTS_FOLDER / 'effluvial.png')

        self.ail_ailment = load_image(ELEMENTS_FOLDER / 'ailment.png')

        self.star = load_image(ELEMENTS_FOLDER / 'star.png')
        self.cross = load_image(ELEMENTS_FOLDER / 'cross.png')

    @classmethod
    def get_instance(cls) -> 'Images':
        """Singleton pattern."""
        if cls.__instance__ is None:
            logger.info('Loading all images except monsters...')
            cls.__instance__ = cls()
            logger.debug('Images loaded!')
        return cls.__instance__
