from typing import List, Tuple, TypedDict, Union

# Here List[int] should be limited to the length of 2
ElementWeakness = Union[List[int], int]


Ailments = List[int]
ElementWeaknesses = List[ElementWeakness]


class MonsterData(TypedDict):
    name: str
    attack: str
    weakness: List[ElementWeakness]
    ailments: Ailments
    color: str


Color = Union[str,
              Tuple[int, int, int],
              Tuple[int, int, int, int]]
